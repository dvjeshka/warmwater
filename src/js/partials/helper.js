//Так приятней :)
window.log = function(param){
    console.log(param);
};


$('.js-styler').styler();

$('.slider').slick({
    });

$('.radio-label2').click(function(){
    $('.items').removeClass('item-col3').addClass('item-col1');
});

$('.radio-label1').click(function(){
    $('.items').removeClass('item-col1').addClass('item-col3');
});

var filterSlider = document.getElementById('filter__slider');
if( filterSlider) {
    noUiSlider.create(filterSlider , {
        start: [500, 1900],
        connect: true,
        range: {
            'min': 0,
            'max': 2500
        }
    });

    var snapValues = [
        document.getElementById('filter__input-left'),
        document.getElementById('filter__input-right')
    ];

    filterSlider.noUiSlider.on('update', function( values, handle ) {
        snapValues[handle].value = ~~values[handle];
    });
}


$('.link-description-more').click(function(e){
    e.preventDefault;
    $(this).hide();
    $('.card__description-more').show();

});



$('.card__nav__link-rev').click(function(e){
    $(this).parent().addClass('card__nav-active').siblings().removeClass('card__nav-active');
    e.preventDefault();
    $('.card__characteristics').hide();
    $('.review').show();
});

$('.card__nav__link-car').click(function(e){
    $(this).parent().addClass('card__nav-active').siblings().removeClass('card__nav-active');
    e.preventDefault();
    $('.card__characteristics').show();
    $('.review').hide();
});

$('.basket-link-confirm').click(function(e){
    e.preventDefault();
    $('.basket__confirm').show();
    $('.basket__items').hide();
});